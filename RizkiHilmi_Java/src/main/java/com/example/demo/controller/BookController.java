package com.example.demo.controller;

import com.example.demo.model.Book;
import com.example.demo.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/book")
public class BookController {

    @Autowired
    BookService bookService ;

    @RequestMapping(value = {"","/"})
    public String listBook(Model model){
        model.addAttribute("Books",bookService.findAll());
        return "books/list";
    }

    @RequestMapping(value = {"/insertbook"},method =RequestMethod.GET)
    public String addBook(Model model) {
        Book book = new Book() ;
        model.addAttribute("Book",book) ;
        return "books/insertbook" ;
    }

    @RequestMapping(value = {"/insertbook"},method =RequestMethod.POST)
    public String saveBook(@ModelAttribute Book book, Model model){
        Book savedBook = bookService.bookSave(book);
        return "redirect:/book" ;
    }
    @RequestMapping(value = {"/editbook/{id}"},method = RequestMethod.GET)
    public String editBook(Model model, @PathVariable("id") Integer id){
        Book book = bookService.findBookById(id);
        model.addAttribute("Book",book) ;
        return "books/editbook" ;
    }
    @RequestMapping(value = {"/deletebook/{id}"},method = RequestMethod.GET)
    public String deleteBook( @PathVariable("id") Integer id){
        bookService.deleteBook(id);
        return "redirect:/book" ;
    }
}
