package com.example.demo.model;

import java.math.BigDecimal;


// digunakan untuk polymorphism
//    public class Mobil {
//     private Integer jumlah ;
//        public String nama ;
//        private BigDecimal harga ;

public class Mobil {
  private String nama ;
  private Integer jumlah ;
  private BigDecimal harga ; //digunakan untuk angka decimal

    public Mobil(String nama) {
        System.out.print("ini adalah constructor mobil dengan nama "+nama+ "\n");
//        this.nama= nama;
    }
    public Mobil() {
        System.out.print("ini adalah constructor mobil.");
    }

    public Mobil(String nama,Integer jumlah,BigDecimal harga){
        System.out.print("ini adalah constructor mobil dengan nama "+nama+", Jumlah " + jumlah+",Harga " +harga+ "\n");
    }

//    public void nama(){
//    }
//
//    public String namaString(){
//        return nama;
//    }
//
//    public String getNama() {
//        return nama;
//    }
//
//    public void setNama(String nama) {
//        this.nama = nama;
//    }
//
//    public Integer getJumlah() {
//        return jumlah;
//    }
//
//    public void setJumlah(Integer jumlah) {
//        this.jumlah = jumlah;
//    }
//
//    public BigDecimal getHarga() {
//        return harga;
//    }
//
//    public void setHarga(BigDecimal harga) {
//        this.harga = harga;
//    }
}
