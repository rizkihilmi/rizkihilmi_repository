package com.example.demo.basic;

import java.util.ArrayList;
import java.util.List;

public class LoopingBilPrima {

    public List<Integer> get(int limit){
            List<Integer> listPrime=new ArrayList<Integer>();
            boolean isPrima;

            for(int i=2;i<=limit;i++){

                isPrima=true;

                for(int j=2;j<i;j++){
                    if(i%j==0){
                        isPrima=false;
                        break;
                    }
                }

                if(isPrima)
                    listPrime.add(i);
            }

            return listPrime;

        }

    }


