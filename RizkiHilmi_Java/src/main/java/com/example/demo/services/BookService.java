package com.example.demo.services;

import com.example.demo.model.Book;
import com.example.demo.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



import java.util.List;

@Service
@Transactional
public class BookService {

    @Autowired
    BookRepository bookRepository ;

    public List<Book> findAll() {
        return bookRepository.findAll();
    }

    public Book bookSave(Book book){
        return bookRepository.save(book) ;
    }

    public Book findBookById(Integer id) {
        return bookRepository.findBookById(id) ;
    }

    public void deleteBook(Integer id) {
        Book book = bookRepository.findBookById(id) ;
        try {
            bookRepository.delete(book);
        }catch (Exception exception) {
            System.out.print(exception.getMessage());
        }
    }

}
