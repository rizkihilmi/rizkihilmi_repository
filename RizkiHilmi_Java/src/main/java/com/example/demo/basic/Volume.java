package com.example.demo.basic;

public class Volume extends Luas{
    private Integer tinggi;
    private Integer sisi;

    public Volume(){

    }

    public Volume(Integer panjang,Integer lebar,Integer tinggi,Integer sisi) {
        super(panjang, lebar);
        this.tinggi = tinggi;
        this.sisi = sisi;
    }
    public Integer hitungvolumebalok(){
        Integer volume = hitungluas() * tinggi;

        return  volume;
    }
    public Integer hitungvolumekubus(){
        Integer volume = sisi * sisi * sisi;

        return  volume;
    }
    public Integer getTinggi() {
    return tinggi;
}

    public void setTinggi(Integer tinggi) {
        this.tinggi = tinggi;
    }

    public Integer getSisi() {
        return sisi;
    }

    public void setSisi(Integer sisi) {
        this.sisi = sisi;
    }
}